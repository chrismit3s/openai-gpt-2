from sys import argv
from textwrap import fill
import gpt_2_simple as gpt2
import os
import tensorflow as tf


# disable compatabilty warning output spam
tf.compat.v1.logging.set_verbosity("ERROR")

models = {
    "S": "124M",
    "M": "355M",
    "L": "774M",
    "XL": "1558M"}

# shamelessly copy-pasted from https://github.com/minimaxir/gpt-2-simple
model_name = models[argv[1].upper()]
if not os.path.isdir(os.path.join("models", model_name)):
    print(f"Downloading {model_name} model...")
    gpt2.download_gpt2(model_name=model_name)  # model is saved into current directory under /models/<size>/

print("Start tensorflow session...")
sess = gpt2.start_tf_sess()
kwargs = {"sess": sess,
          "run_name": f"run_{model_name}"}

print("Loading model...")
gpt2.load_gpt2(**kwargs)

print("Generating...")
generated = gpt2.generate(prefix="The best meme is",
                          length=100,
                          nsamples=1,
                          temperature=0.6,
                          batch_size=1,
                          return_as_list=True,
                          **kwargs)[0]

print("\n".join(fill(line) for line in generated.splitlines()))
